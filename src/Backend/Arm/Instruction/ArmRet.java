package Backend.Arm.Instruction;

import Backend.Arm.Operand.ArmCPUReg;
import Backend.Arm.Operand.ArmReg;
import Backend.Riscv.Operand.RiscvReg;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class ArmRet extends ArmInstruction {
    public ArmRet(ArmReg armReg) {
        super(null, new ArrayList<>(Collections.singletonList(armReg)));
        assert armReg == ArmCPUReg.getArmRetReg();
    }

    @Override
    public String toString() {
        return "bx" + " " + getOperands().get(0);
    }
}
