package Pass.IR;

import IR.IRModule;
import IR.Value.*;
import IR.Value.Instructions.*;
import Pass.IR.Utils.AccessAnalysis;
import Pass.Pass;
import Utils.DataStruct.IList;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Stack;
import java.util.concurrent.atomic.AtomicBoolean;

class ArrayInfo {
    public ArrayList<Instruction> mems = new ArrayList<>();
    public LinkedHashMap<Value, StoreInst> memout = new LinkedHashMap<>();
    public LinkedHashMap<Value, LinkedHashSet<StoreInst>> memin = new LinkedHashMap<>();

    // 保存到达此处的func参数ptr的来源块，代表这些value会被修改，可以认为是store
    public LinkedHashMap<Value, LinkedHashSet<BasicBlock>> memFuncsin =
            new LinkedHashMap<>();

    public LinkedHashSet<Value> memFuncsout = new LinkedHashSet<>();
}


public class ArrayEliminate implements Pass.IRPass {

    LinkedHashMap<BasicBlock, ArrayInfo> blockArrayInfos;
    LinkedHashMap<StoreInst, Integer> storeCount;
    LinkedHashSet<Value> ignoredArray;
    public boolean needrepeat;

    @Override
    public String getName() {
        return "ArrayEliminate";
    }

    @Override
    public void run(IRModule module) {
        needrepeat = false;
        ignoredArray = new LinkedHashSet<>();
        blockArrayInfos = new LinkedHashMap<>();
        storeCount = new LinkedHashMap<>();
        AtomicBoolean canrun = new AtomicBoolean(true);
        module.functions().forEach(f -> {
            f.getBbs().forEach(bbnode -> {
                blockArrayInfos.put(bbnode.getValue(), new ArrayInfo());
                bbnode.getValue().getInsts().forEach(instnode ->{
                    if(instnode.getValue() instanceof PtrInst ptrinst){
                        if(ptrinst.getTarget() instanceof PtrInst){
                            canrun.set(false);
                        }
                    }
                });
            });
            if(f.getBbs().getSize()>500){
                canrun.set(false);
            }
        });
        if(!canrun.get()){
            return;
        }

//        InterproceduralAnalysis.run(module);
        ignoreArrayAnalysis(module); //识别跨函数数组交叉访问
        AccessAnalysis.run(module);

        module.functions().forEach(this::runForFunction);

        //这里的逻辑是如果替换了一些Load，需要重新分析Store们是否被用到。如果没有新替换的Load，则可以用StoreCount清除Store
        if(!needrepeat){
            ArrayList<Instruction> needRemove = new ArrayList<>();
            storeCount.keySet().forEach(s -> {
                if(s.getPointer() instanceof PtrInst){
                    if (storeCount.get(s) == 0 &&
                            !(((PtrInst) s.getPointer()).getTarget() instanceof Argument ||
                                    ((PtrInst) s.getPointer()).getTarget() instanceof GlobalVar)) {
                        needRemove.add(s);
                    }
                }else if(s.getPointer() instanceof AllocInst && storeCount.get(s)==0){
                    needRemove.add(s);
                }
            });
            for (Instruction instruction : needRemove) {
                instruction.removeSelf();
            }
            if (!needRemove.isEmpty()) {
                needrepeat = true;
            }
        }
        return;
    }

    private void ignoreArrayAnalysis(IRModule module) {
        LinkedHashSet<Value> visited = new LinkedHashSet<>();
        for (Function function : module.functions()) {
            LinkedHashSet<Value> visitedCur = new LinkedHashSet<>();
            for (IList.INode<BasicBlock, Function> node : function.getBbs()) {
                var bb = node.getValue();
                for (IList.INode<Instruction, BasicBlock> instnode : bb.getInsts()) {
                    var inst = instnode.getValue();
                    if (inst instanceof PtrInst ptrInst) {
                        visitedCur.add(ptrInst.getTarget());
                    } else if (inst instanceof CallInst call && !call.getFunction().isLibFunction()) {
                        for (var ope : call.getOperands()) {
                            if (ope instanceof PtrInst) {
                                ignoredArray.add(((PtrInst) ope).getTarget());
                            }
                        }
                    }
                }
            }
            for (var arr : visitedCur) {
                if (!visited.contains(arr)) {
                    visited.add(arr);
                } else {
                    ignoredArray.add(arr);
                }
            }
        }
    }


    ArrayList<Value> getPtrOffset(PtrInst ptrInst) {
        var ret = new ArrayList<Value>();
        Value offset = ptrInst.getOffset();
//        ptrInst.getTarget()
        return ret;
    }

    private boolean mustPass(Function f, BasicBlock from, BasicBlock to, BasicBlock mid) {
        assert !mid.equals(to);
        var dom = f.getDomer();
        return (dom.get(mid).contains(from) && dom.get(to).contains(from) &&
                dom.get(to).contains(mid));
    }

    private boolean isStaticSpace(Value v){
        return v instanceof AllocInst || v instanceof GlobalVar;
    }


    private boolean mayConflict(Value target1, Value target2, boolean isFunc){
        if(target1 instanceof PtrInst ptr1 && target2 instanceof PtrInst ptr2){
            if(ptr1.getTarget() instanceof PtrInst || ptr2.getTarget() instanceof PtrInst){
                return true;
            }
            if(ptr1.getOffset() instanceof ConstInteger && ptr2.getOffset() instanceof ConstInteger &&
                    !ptr1.getOffset().equals(ptr2.getOffset())){
                return false;
            }
            if(isStaticSpace(ptr1.getTarget()) && isStaticSpace(ptr2.getTarget()) &&
                    !ptr1.getTarget().equals(ptr2.getTarget())){
                return false;
            }
            return true;
        }else if(target1 instanceof GlobalVar && target2 instanceof GlobalVar){
            if(!target1.equals(target2)){
                return false;
            }
            return true;
        }else if((target1 instanceof AllocInst && target2 instanceof PtrInst) ||
                (target1 instanceof PtrInst && target2 instanceof AllocInst)){
            AllocInst alloca=null;
            PtrInst ptr= null;
            if(target1 instanceof AllocInst alloca1){
                alloca = alloca1;
                ptr = (PtrInst) target2;
            }else{
                alloca = (AllocInst) target2;
                ptr = (PtrInst) target1;
            }
            if(ptr.getTarget().equals(alloca) &&
                    ptr.getOffset() instanceof ConstInteger constindex && constindex.getValue()!=0){
                return isFunc;
            }
        }else if(target1 instanceof AllocInst && target2 instanceof AllocInst){
            if(target1.equals(target2))return true;
            return false;
        }

        Value targetSpace = target1;
        if(target1 instanceof PtrInst){
            targetSpace = ((PtrInst) target1).getTarget();
        }
        Value target2Space = target2;
        if(target2 instanceof PtrInst){
            target2Space = ((PtrInst) target2).getTarget();
        }
        if(targetSpace.equals(target2Space)){
            return true;
        }
        return false;
    }

    private void isSamePosition(Value ptr1, Value ptr2){}//todo


    private void runForFunction(Function f) {
        if (f.isLibFunction()) return;
        var pastcount = storeCount;
        storeCount = new LinkedHashMap<>();
        boolean hasRecursive = false;
        for (IList.INode<BasicBlock, Function> bbnode : f.getBbs()) {
            hasRecursive = hasRecursive || analysisForBlockReturnRecur(bbnode.getValue());
        }
        if (hasRecursive) {
            var stores = new ArrayList<>(storeCount.keySet());
            for (StoreInst store : stores) {
                storeCount.put(store, 1);
            }
            // 认为所有store都被用过。
        }
        storeCount.putAll(pastcount);
        // 更新memin
        for (IList.INode<BasicBlock, Function> bbnode : f.getBbs()) {
            var bb = bbnode.getValue();
            //遍历每个块
            var bbinfo = blockArrayInfos.get(bb);
            for (var accblock : bb.accessible) {
                //遍历这个块的可达块
                var accinfo = blockArrayInfos.get(accblock);
                for (var ptr : bbinfo.memout.keySet()) {
                    //对每个来源块的memout，检查能否添加到accblock的in中
                    var store = bbinfo.memout.get(ptr);

                    boolean hasPhi = false;
                    if(ptr instanceof PtrInst ptradd){
                        Stack<Value> checklist = new Stack<>();
                        checklist.add(ptradd.getOffset());
                        while(!checklist.isEmpty()){
                            var check = checklist.pop();
                            if(check instanceof Phi){
                                hasPhi = true;
                                break;
                            }
                            if(check instanceof User){
                                checklist.addAll(((User) check).getOperands());
                            }
                        }
                    }
                    boolean needAdd = true;
                    ArrayList<StoreInst> needDelete = new ArrayList<>();
                    //如果包含了相同的ptr，检查只配关系，如果互相有遮蔽关系，则进行遮蔽。
                    if (accinfo.memin.containsKey(ptr) && !hasPhi) {
                        for (var accs : accinfo.memin.get(ptr)) {
                            var fromblock = accs.getParentbb();
                            if (bb.equals(accblock) || fromblock.equals(accblock)) continue;
                            // 来源于自身的store不能被shed，也不能shed其他store。
                            if (fromblock.equals(bb)) {
                                accs.getParentbb().getInsts().updateIndex();
                                if (store.getNode().getIndexInList() > accs.getNode().getIndexInList()) {
                                    needDelete.add(accs);
                                    continue;
                                } else {
                                    needAdd = false;
                                    break;
                                }
                            }
                            // 三个均不等

                            //fromblock到达目标一定会经过bb
                            if (mustPass(f, fromblock, accblock, bb)) {
                                needDelete.add(accs);
                            } else if (mustPass(f, bb, accblock, fromblock)) {
                                needAdd = false;
                                break;
                            }
                        }
                        needDelete.forEach(accinfo.memin.get(ptr)::remove);
                    }
                    if (needAdd){
                        if(!accinfo.memin.containsKey(ptr)){
                            accinfo.memin.put(ptr, new LinkedHashSet<>());
                        }
                        accinfo.memin.get(ptr).add(store);
                    }
                }
                for(var funcptr : bbinfo.memFuncsout){
                    if(!accinfo.memFuncsin.containsKey(funcptr)){
                        accinfo.memFuncsin.put(funcptr, new LinkedHashSet<>());
                    }
                    accinfo.memFuncsin.get(funcptr).add(bb);
                }
            }
        }

        //所有可到达自身的store和memfunc都设置到对应的accinfo了
        for(var bbnode : f.getBbs()){
            var bb = bbnode.getValue();
            var info = blockArrayInfos.get(bb);
            var visitedStores = new ArrayList<StoreInst>();
            for(var inst : info.mems){
                if(inst instanceof LoadInst load){
                    LinkedHashSet<StoreInst> conflictStores = new LinkedHashSet<>();
                    Value ptr = load.getPointer();
                    for (Value ptrInst : info.memin.keySet()) {
                        if(mayConflict(ptrInst, ptr, false)){
                            conflictStores.addAll(info.memin.get(ptrInst));
                        }
                    }
                    //插入到达这个load的所有可能与ptrInst冲突的store
                    //如果值可能来源于被函数修改的数组，直接认为所有冲突store都可能被用到，不再判断，跳过
                    boolean conflictWithFunc = false;
                    for(var funcptr : info.memFuncsin.keySet()){
                        conflictWithFunc = conflictWithFunc || mayConflict( funcptr, ptr, true);
                    }
                    if(conflictWithFunc){
                        conflictStores.forEach(s->storeCount.put(
                                s, storeCount.get(s)+1
                        ));
                        continue;
                    }
                    //保存块中从前往后遍历已经遇到的store，用来判断load替换使用。
                    //判断是否有store使用了同索引，并判断这个store是否必到达load、是否不会被其他store覆盖。
                    // 如果store来源于自身块，则需要判断是否在当前inst之前。
                    if(info.memin.containsKey(ptr) && !info.memin.get(ptr).isEmpty()){
                        if(info.memin.get(ptr).size() == 1){
                            StoreInst s0 = info.memin.get(ptr).iterator().next();
                            //mid 是可能替换当前load的store来源block
                            BasicBlock mid = s0.getParentbb();
                            if(!f.getDomer().get(bb).contains(mid) ||
                                    (mid.equals(bb) && !visitedStores.contains(s0))){
                                conflictStores.forEach(s->storeCount.put(
                                        s, storeCount.get(s)+1
                                ));
                                continue;
                            }
                            boolean canreplace = true;
                            for(var s : conflictStores){
                                if(visitedStores.contains(s0) && !visitedStores.contains(s)){
                                    continue;
                                }
                                BasicBlock fromb = s.getParentbb();
                                if(!mid.equals(fromb)){
                                    //这个冲突store是否被可能替换store拦截，没拦截则无法替换
                                    if(!mustPass(f, fromb, bb, mid)){
                                        canreplace = false;
                                        break;
                                    }
                                }else{
                                    //在同一个块中，则判断这个冲突store是否在可能替换store前面，如果在后面则无法替换。
                                    mid.getInsts().updateIndex();
                                    if(s0.getNode().getIndexInList() < s.getNode().getIndexInList()){
                                        canreplace = false;
                                        break;
                                    }
                                }

                            }
                            if(canreplace){
                                load.replaceUsedWith(s0.getValue());
                                load.removeSelf();
                                needrepeat = true;
                                continue;
                            }
                        }
                        conflictStores.forEach(s->storeCount.put(
                                s, storeCount.get(s)+1
                        ));
                    }else {
                        if(!conflictStores.isEmpty()){
                            conflictStores.forEach(s->storeCount.put(
                                    s, storeCount.get(s)+1
                            ));
                        }
                    }
                } else if (inst instanceof StoreInst s0) {
                    visitedStores.add(s0);
                    //如果是重复ptr，覆盖所有原值，直接替换。
                    Value ptr = s0.getPointer();
                    boolean staticAddress = true;
                    if(ptr instanceof PtrInst && !(((PtrInst) ptr).getOffset() instanceof ConstInteger)){
                        staticAddress = false;
                    }
                    if(staticAddress){
                        if (info.memin.containsKey(ptr)) {
                            info.memin.get(ptr).clear();
                        }
                    }
                    if (!info.memin.containsKey(ptr)) {
                        info.memin.put(ptr, new LinkedHashSet<>());
                    }
                    info.memin.get(ptr).add(s0);
                } else if (inst instanceof CallInst call) {
                    //如果是call，加入memFuncsin即可，需要在Load时检查是否被覆盖，再做出替换决定。
                    //同时，涉及到的有冲突的store认为被用到了
                    boolean safefunc = false;
                    if (call.getFunction().getName().equals("@memset") ||
                            call.getFunction().getName().equals("@putarray") ||
                            call.getFunction().getName().equals("@putfarray")) {
                        safefunc = true;
                    }
                    if(!safefunc){
                        for (var ope : call.getOperands()) {
                            if (ope instanceof PtrInst || ope instanceof GlobalVar || ope instanceof AllocInst) {
                                if(!info.memFuncsin.containsKey(ope)){
                                    info.memFuncsin.put(ope, new LinkedHashSet<>());
                                }
                                info.memFuncsin.get(ope).add(bb);
                            }
                        }
                    }
                    for(var ope : call.getOperands()){
                        if(ope instanceof PtrInst || ope instanceof GlobalVar || ope instanceof AllocInst){
                            for(var key : info.memin.keySet()){
                                if(mayConflict(key, ope, true)){
                                    info.memin.get(key).forEach(e->storeCount.put(e, storeCount.get(e)+1));
                                }
                            }
                        }
                    }
                }
            }
        }

    }

    private boolean analysisForBlockReturnRecur(BasicBlock block) {
        boolean recur = false;
        var info = blockArrayInfos.get(block);
        for (IList.INode<Instruction, BasicBlock> instnode : block.getInsts()) {
            var inst = instnode.getValue();
            if (inst instanceof StoreInst store) {
                if (store.getPointer() instanceof GlobalVar global){
                    if(ignoredArray.contains(global)){
                        continue;
                    }
                    info.memout.put(global, store);
                    storeCount.put(store, 0);
                    info.mems.add(store);
                }
                else if (store.getPointer() instanceof PtrInst ptrinst) {
                    if (ignoredArray.contains(ptrinst.getTarget())) {
                        continue;
                    }
                    info.memout.put(ptrinst, store);
                    storeCount.put(store, 0);
                    info.mems.add(store);
                }else if(store.getPointer() instanceof AllocInst){
                    info.memout.put(store.getPointer(), store);
                    storeCount.put(store, 0);
                    info.mems.add(store);
                }
            } else if (inst instanceof CallInst call) {
                if (call.getFunction().getName().equals("@memset") ||
                        call.getFunction().getName().equals("@putarray") ||
                        call.getFunction().getName().equals("@putfarray")) {
                    info.mems.add(call);
                    continue;
                }
                for (var ope : call.getOperands()) {
                    if (ope instanceof PtrInst || ope instanceof GlobalVar || ope instanceof AllocInst) {
                        info.memFuncsout.add(ope);
                    }
                }
                if (((CallInst) inst).getFunction().getName().equals(block.getParentFunc().getName())) {
                    recur = true;
                }
                info.mems.add(call);
            } else if (inst instanceof LoadInst load) {
                if (!(load.getPointer() instanceof PtrInst || load.getPointer() instanceof GlobalVar || load.getPointer() instanceof AllocInst)) {
                    continue;
                }
                if (load.getPointer() instanceof PtrInst &&
                        ignoredArray.contains(((PtrInst) load.getPointer()).getTarget())) {
                    continue;
                }
                if (load.getPointer() instanceof GlobalVar &&
                        ignoredArray.contains(load.getPointer())) {
                    continue;
                }
                info.mems.add(load);
            }
        }
        return recur;
    }


}
