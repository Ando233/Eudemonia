package Pass.IR;

import Frontend.AST;
import IR.IRBuildFactory;
import IR.IRModule;
import IR.Type.IntegerType;
import IR.Value.*;
import IR.Value.Instructions.*;
import Pass.IR.Utils.DomAnalysis;
import Pass.IR.Utils.LoopAnalysis;
import Pass.IR.Utils.UtilFunc;
import Pass.Pass;
import Utils.DataStruct.IList;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.stream.BaseStream;

//  PeepHole中都是一些对特定模式的优化
public class PeepHole implements Pass.IRPass {

    private final IRBuildFactory f = IRBuildFactory.getInstance();

    @Override
    public void run(IRModule module) {
        //  ptradd A, 0
        PeepHole1(module);
        //  br %x, %block A, %block A
        PeepHole2(module);
        //  br %x %block A, %block B
        //  Block B: br %x %block C, %block D
        PeepHole3(module);
        //  store A ptr1; ...; B = load ptr1;
        PeepHole4(module);
        LoopPeepHole(module);
        FunctionPeephole(module);
        ArithmeticPeephole(module);
    }

    private void ArithmeticPeephole(IRModule module) {
        for (Function function : module.functions()) {
            if (function.getBbs().getSize() != 7) continue ;
            BasicBlock entry = function.getBbEntry();
            if (entry.getInsts().getSize() != 2) continue ;
            boolean recur = false;
            for (Function caller : function.getCallerList()) {
                if (caller == function) {
                    recur = true;
                    break;
                }
            }
            if (!recur) continue;
            ArrayList<CallInst> callInsts = new ArrayList<>();
            ArrayList<RetInst> retInsts = new ArrayList<>();
            for (IList.INode<BasicBlock, Function> bbNode : function.getBbs()) {
                BasicBlock bb = bbNode.getValue();
                for (IList.INode<Instruction, BasicBlock> instNode : bb.getInsts()) {
                    Instruction inst = instNode.getValue();
                    if (inst instanceof CallInst callInst) {
                        callInsts.add(callInst);
                    }
                    else if (inst instanceof RetInst retInst) {
                        retInsts.add(retInst);
                    }
                }
            }
            if (callInsts.size() > 1 || retInsts.size() < 3) return ;
            CallInst callInst = callInsts.get(0);
            if (callInst.getParams().size() != 2) return ;
            Value arg = callInst.getParams().get(0);
            Value divInst = callInst.getParams().get(1);
            if (arg != function.getArgs().get(0)) return ;
            if (!(divInst instanceof BinaryInst binInst) || binInst.getOp() != OP.Div) return ;
            if (((BinaryInst) divInst).getLeftVal() != function.getArgs().get(1)
                    || !(((BinaryInst) divInst).getRightVal() instanceof ConstInteger constInt)
                    || constInt.getValue() != 2) return ;
            if (entry.getInsts().getSize() != 2) return ;
            BasicBlock divBb = ((BinaryInst) divInst).getParentbb();
            if (divBb.getInsts().getSize() != 7) return ;
            for (Value user : divInst.getUserList()) {
                if (user != callInst) return ;
            }
            ArrayList<BinaryInst> addInsts = new ArrayList<>();
            for (Value user : callInst.getUserList()) {
                if (!(user instanceof BinaryInst addInst) || addInst.getOp() != OP.Add) return ;
                addInsts.add(addInst);
            }
            if (addInsts.size() != 1) return ;
            BinaryInst addInst = addInsts.get(0);
            if (addInst.getLeftVal() != callInst || addInst.getRightVal() != callInst) return ;
            ArrayList<BinaryInst> divInsts = new ArrayList<>();
            for (Value user : addInst.getUserList()) {
                if (user instanceof BinaryInst divInst2 && divInst2.getOp() != OP.Div) {
                    divInsts.add(divInst2);
                }
            }
            if (divInsts.size() != 1) return ;
            if (!(divInsts.get(0).getRightVal() instanceof ConstInteger constInt2)) return ;
            BasicBlock preHead = f.getBasicBlock(function);
            Value mod = constInt2;
            BinaryInst mulInst = f.buildBinaryInst(function.getArgs().get(0), function.getArgs().get(1), OP.Mul, preHead);
            mulInst.I64 = true;
            BinaryInst modInst = f.buildBinaryInst(mulInst, mod, OP.Mod, preHead);
            modInst.I64 = true;
            f.buildRetInst(modInst, preHead);
            preHead.insertBefore(function.getBbEntry());
        }
    }

    //block216:
    //	%548 = phi i32 [ 0, %block210 ],[ %554, %block216 ]     //01
    //	%549 = phi i32 [ 0, %block210 ],[ %559, %block216 ]     //02
    //	%1359 = add i32 4950, %549                              //03
    //	%559 = srem i32 %1359, 65535                            //04
    //	%554 = add i32 %548, 1                                  //05
    //	%550 = icmp slt i32 %554, %532                          //06
    //	br i32 %550, label %block216, label %block420           //07
    private void LoopPeepHole(IRModule module) {
        for (var func : module.functions()) {
            boolean changed = false;
            LoopAnalysis.runLoopInfo(func);
            for (var loop : func.getAllLoops()) {
                Instruction instruction3 = null;
                Instruction instruction4 = null;
                Instruction instruction5 = null;
                Instruction instruction6 = null;

                if (loop.getBbs().size() != 1) {
                    continue;
                }
                var bb = loop.getBbs().get(0);
                if (bb.getInsts().getSize() != 7) {
                    continue;
                }
                ArrayList<Phi> allPhi = new ArrayList<>();
                ArrayList<Instruction> allInsts = new ArrayList<>();
                //遍历bb，添加到allphi
                for (IList.INode<Instruction, BasicBlock> instNode : bb.getInsts()) {
                    Instruction inst = instNode.getValue();
                    if (inst instanceof Phi phi) {
                        allPhi.add(phi);
                    }
                    allInsts.add(inst);
                }
                if (allPhi.size() != 2 || allPhi.get(1).getOperands().size() != 2) {
                    continue;
                }
                var brcond = bb.getBrCond(); //06
                if (brcond == null || brcond.getOp() != OP.Lt) {
                    continue;
                }
                if (!(allInsts.contains(brcond.getOperand(0)) && !allInsts.contains(brcond.getOperand(1)))) {
                    continue;
                }
                //startIndex to endIndex, +1 every time
                // startAddingValue = (startAddingValue + addingNumber) % remNumber

                Phi indexPhi = null;
                ConstInteger startIndexConst = null;
                Value endIndex = null;

                Phi addingPhi = null;
                Value startAddingValue = null;
                ConstInteger addingNumber = null;
                ConstInteger remNumber = null;

                endIndex = brcond.getOperand(1);

                boolean successMap = true;
                for (var phi : allPhi) {
                    var value2bb = phi.getPhiValues();
                    LinkedHashMap<Value, BasicBlock> valuemap = new LinkedHashMap<>();
                    for (var pair : value2bb) {
                        valuemap.put(pair.getFirst(), pair.getSecond());
                    }
                    boolean hasConstIncome = false;
                    ConstInteger constvalue = null;
                    Value nonConstValue = null;
                    for (var value : valuemap.keySet()) {
                        if (value instanceof ConstInteger) {
                            hasConstIncome = true;
                            constvalue = (ConstInteger) value;
                        } else {
                            nonConstValue = value;
                        }
                    }
                    if (!hasConstIncome) {
                        successMap = false;
                        break;
                    }
                    if (!(nonConstValue instanceof BinaryInst binaryInst)) {
                        successMap = false;
                        break;
                    }
                    //match index adding pattern
                    //01
                    if (binaryInst.getOp() == OP.Add &&
                            binaryInst.getOperand(0).equals(phi) &&
                            (binaryInst.getOperand(1) instanceof ConstInteger constInteger &&
                                    constInteger.getValue() == 1)) {
                        indexPhi = phi;
                        startIndexConst = constvalue;
                        instruction5 = binaryInst;
                        // binaryInst is 05
                        if (!brcond.getOperand(0).equals(binaryInst)) { // 判断这个自增1的phi的binaryinst是否是icmp的第一个操作数
                            successMap = false;
                            break;
                        }
                    }
                    //02
                    else if (binaryInst.getOp() == OP.Mod &&
                            binaryInst.getOperand(1) instanceof ConstInteger) {
                        instruction4 = binaryInst;
                        //binaryInst is 04
                        remNumber = (ConstInteger) binaryInst.getOperand(1);
                        addingPhi = phi;
                        startAddingValue = constvalue;
                        if (!(binaryInst.getOperand(0) instanceof BinaryInst subbinaryInst &&
                                allInsts.contains(subbinaryInst))) {
                            successMap = false;
                            break;
                        }
                        instruction3 = subbinaryInst;
                        //subBinaryInst is 03
                        if (subbinaryInst.getOperand(0) instanceof ConstInteger subconst&&
                                subbinaryInst.getOperand(1) instanceof Phi subusephi&&
                                allInsts.contains(subusephi) && subusephi.getOperands().contains(binaryInst)
                        ) {
                            addingNumber = subconst;
                        }else if(
                                subbinaryInst.getOperand(1) instanceof ConstInteger subconst&&
                                        subbinaryInst.getOperand(0) instanceof Phi subusephi&&
                                        allInsts.contains(subusephi) && subusephi.getOperands().contains(binaryInst)
                        ){
                            addingNumber = subconst;
                        }else{
                            successMap = false;
                            break;
                        }
                    }
                }
                if (!successMap) {
                    continue;
                }
                for(var inst : allInsts){
                    for(User user : inst.getUserList()){
                        if(!(allInsts.contains(user)) && !inst.equals(instruction4)){
                            successMap = false;
                        }
                    }
                }
                if (!successMap) {
                    continue;
                }

                //build and replace instruction4 by result;
                BrInst brinst = (BrInst) bb.getLastInst();
                if(! brinst.getTrueBlock().equals(bb)){
                    continue;
                }
                var times = new BinaryInst(OP.Sub, brcond.getOperand(1) ,startIndexConst, startIndexConst.getType());
                var modtimes = new BinaryInst(OP.Mod, times, remNumber, remNumber.getType());
                var addinsRes = new BinaryInst(OP.Mul, modtimes, addingNumber, addingNumber.getType());
                var addingStartRes = new BinaryInst(OP.Add, addinsRes, startAddingValue, startAddingValue.getType());
                var modRes = new BinaryInst(OP.Mod, addingStartRes, remNumber, remNumber.getType());
                for(var inst : allInsts){
                    inst.removeSelf();
                }
                bb.addInst(times);
                bb.addInst(modtimes);
                bb.addInst(addinsRes);
                bb.addInst(addingStartRes);
                bb.addInst(modRes);
                bb.addInst(new BrInst(brinst.getFalseBlock()));
                instruction4.replaceUsedWith(modRes);
                changed  = true;
            }
            if(changed){
                UtilFunc.makeCFG(func);
            }
        }

    }

    private void FunctionPeephole(IRModule module){
        Function matchFunc = null;
        int dataIndex = 0;
        int recurIndex = 0;
        //block14:
        //	%16 = icmp slt i32 %num_param, 0
        //	br i32 %16, label %block9, label %block10
        //block9:
        //	ret float 0.0
        //block10:
        //	%20 = sub i32 %num_param, 1
        //	%24 = call float @func(float %data_param, i32 %20)
        //	%25 = fadd float %data_param, %24
        //	%29 = call float @func(float %25, i32 %20)
        //	%30 = fsub float %25, %29
        //	ret float %30

        // 模式识别：三个块、entry+entry的两个后继，两个后继的lastInst都是ret，两个arg
        // 要求entry两条语句，brcond判断其中一个arg小于0，认为其是recurIndex，另一个是data
        //  yesblock return 0.0
        //  no block return (data+call(data))-call(newdata)，newdata = data+call(data)
        //  no block both call use recurIndex-1 to call



        for(var func : module.functions()){
            if(func.getBbs().getSize()!=3 || func.getBbs().getHead().getValue().getNxtBlocks().size()!=2 ||
                !func.getBbs().getHead().getNext().getValue().getNxtBlocks().isEmpty() ||
                    !func.getBbs().getHead().getNext().getNext().getValue().getNxtBlocks().isEmpty()){
                continue;
            }
            boolean returnInt = func.getType() instanceof IntegerType;
            var entry = func.getBbEntry();
            BrInst br = (BrInst) entry.getLastInst();
            var cond = entry.getBrCond();
            if(!(entry.getInsts().getSize()==2)){
                continue;
            }
            if(!(cond.getOperand(0) instanceof Argument curIndex &&
                    cond.getOperand(1) instanceof ConstInteger curStopcond && curStopcond.getValue() == 0 &&
                    cond.getOp()==OP.Lt)){
                continue;
            }
            Argument data = null;
            for(var arg : func.getArgs()){
                if(!arg.equals(curIndex)){
                    data = arg;
                }
            }
            if(data == null){
                continue;
            }
            var stopBlock = br.getTrueBlock();
            var recurBlock = br.getFalseBlock();
            if(!(stopBlock.getInsts().getSize()==1 && stopBlock.getLastInst() instanceof RetInst ret &&
                    (returnInt?
                            ret.getOperand(0) instanceof ConstInteger retval &&
                                    retval.getValue() == 0 :
                            ret.getOperand(0) instanceof ConstFloat retfloatval &&
                                    retfloatval.getValue()==(float) 0.0
                    ))){
                continue;
            }
            if(!(recurBlock.getInsts().getSize()==6)){
                continue;
            }

            //	%20 = sub i32 %num_param, 1
            //	%24 = call float @func(float %data_param, i32 %20)
            //	%25 = fadd float %data_param, %24
            //	%29 = call float @func(float %25, i32 %20)
            //	%30 = fsub float %25, %29
            //	ret float %30
            var recurRet = ((RetInst)recurBlock.getLastInst()).getOperand(0);
            var newRecurIndex = recurBlock.getInsts().getHead().getValue();
            if(!(newRecurIndex instanceof BinaryInst bina && newRecurIndex.getOp()==OP.Sub &&
                    bina.getRightVal() instanceof ConstInteger subval && subval.getValue() == 1 &&
                    bina.getLeftVal().equals(curIndex))){
                continue;
            }
            if(!(newRecurIndex.getNode().getNext().getValue() instanceof CallInst firstCall &&
                    firstCall.getParams().contains(newRecurIndex) && firstCall.getParams().contains(data))){
                continue;
            }
            if(firstCall.getOperand(func.getArgs().indexOf(data)) != data){
                continue;
            }
            var retOpe = firstCall.getNode().getNext().getValue();
            if(!(retOpe instanceof BinaryInst retBinary && retBinary.getOp() == OP.Fadd &&
                    retBinary.getOperands().contains(firstCall) && retBinary.getOperands().contains(data))){
                continue;
            }
            var maysecondCall = retOpe.getNode().getNext().getValue();
            if(!(maysecondCall instanceof CallInst secondCall &&
                    secondCall.getOperands().contains(retOpe) && secondCall.getOperands().contains(newRecurIndex))){
                continue;
            }
            var finalOpe = secondCall.getNode().getNext().getValue();
            if(!(finalOpe instanceof BinaryInst finalBinary && finalBinary.getOp() == OP.Fsub &&
                    finalBinary.getLeftVal().equals(retOpe) && finalBinary.getRightVal().equals(secondCall))){
                continue;
            }
            if(!recurRet.equals(finalOpe)){
                continue;
            }
            matchFunc = func;
            dataIndex = func.getArgs().indexOf(data);
            recurIndex = func.getArgs().indexOf(curIndex);
            break;

        }
        if(matchFunc==null){
            return;
        }
        ArrayList<BasicBlock> matchbbs= new ArrayList<>();
        for(var bn : matchFunc.getBbs()){
            matchbbs.add(bn.getValue());
        }
        for(var bb : matchbbs){
            bb.removeSelf();
        }
        BasicBlock newentry = new BasicBlock(matchFunc);
        BasicBlock newZeroBlock = new BasicBlock(matchFunc);
        BasicBlock newDataBlockl = new BasicBlock(matchFunc);
        newZeroBlock.addInst(new RetInst(new ConstFloat((float)0.0)));
        newDataBlockl.addInst(new RetInst(matchFunc.getArgs().get(dataIndex)));
        var newmod = new BinaryInst(OP.Mod,
                matchFunc.getArgs().get(recurIndex), new ConstInteger(2, IntegerType.I32),
                IntegerType.I1);
        var newCond = new BinaryInst(OP.Eq,
                newmod, new ConstInteger(1, IntegerType.I1),
                IntegerType.I1);
        var newBr = new BrInst(newCond, newZeroBlock, newDataBlockl);
        newentry.addInst(newmod);
        newentry.addInst(newCond);
        newentry.addInst(newBr);
        matchFunc.getBbs().add(newentry.getNode());
        matchFunc.getBbs().add(newZeroBlock.getNode());
        matchFunc.getBbs().add(newDataBlockl.getNode());
        UtilFunc.makeCFG(matchFunc);
        new FuncInLine().run(module);
    }

    private void PeepHole3(IRModule module) {
        for (Function function : module.functions()) {
            if (function.isLibFunction()) continue;
            for (IList.INode<BasicBlock, Function> bbNode : function.getBbs()) {
                BasicBlock bb = bbNode.getValue();
                Instruction lastInst = bb.getLastInst();
                if (lastInst instanceof BrInst brInst
                        && brInst.getJudVal() != null) {
                    Value value = brInst.getJudVal();
                    BasicBlock trueBb = brInst.getTrueBlock();
                    BasicBlock falseBb = brInst.getFalseBlock();
                    Instruction falseFirInst = falseBb.getFirstInst();
                    Instruction trueFirInst = trueBb.getFirstInst();
                    if (falseFirInst instanceof BrInst falseBr
                            && falseBr.getJudVal() == value) {
                        BasicBlock falseNxt = falseBr.getFalseBlock();
                        brInst.replaceBlock(falseBb, falseNxt);
                        falseBb.removePreBlock(bb);
                        bb.removeNxtBlock(falseBb);
                        bb.setNxtBlock(falseNxt);
                        falseNxt.setPreBlock(bb);
                        int idx = falseNxt.getPreBlocks().indexOf(falseBb);
                        for (IList.INode<Instruction, BasicBlock> instNode : falseNxt.getInsts()) {
                            Instruction phiInst = instNode.getValue();
                            if (phiInst instanceof Phi phi) {
                                phi.addOperand(phi.getOperand(idx));
                            }
                            else break;
                        }
                    }
                    else if (trueFirInst instanceof BrInst trueBr
                            && trueBr.getJudVal() == value) {
                        BasicBlock trueNxt = trueBr.getTrueBlock();
                        brInst.replaceBlock(trueBb, trueNxt);
                        trueBb.removePreBlock(bb);
                        bb.removeNxtBlock(trueBb);
                        bb.setNxtBlock(trueNxt);
                        trueNxt.setPreBlock(bb);
                        int idx = trueNxt.getPreBlocks().indexOf(trueBb);
                        for (IList.INode<Instruction, BasicBlock> instNode : trueNxt.getInsts()) {
                            Instruction phiInst = instNode.getValue();
                            if (phiInst instanceof Phi phi) {
                                phi.addOperand(phi.getOperand(idx));
                            }
                            else break;
                        }
                    }

                }
            }
        }
    }

    private void PeepHole1(IRModule module) {
        for (Function function : module.functions()) {
            for (IList.INode<BasicBlock, Function> bbNode : function.getBbs()) {
                BasicBlock bb = bbNode.getValue();
                for (IList.INode<Instruction, BasicBlock> instNode : bb.getInsts()) {
                    Instruction inst = instNode.getValue();
                    if (inst instanceof PtrInst ptrInst
                            && ptrInst.getOffset() instanceof ConstInteger constInteger
                            && constInteger.getValue() == 0) {
                        ptrInst.replaceUsedWith(ptrInst.getTarget());
                        ptrInst.removeSelf();
                    }
                }
            }
        }
    }

    private void PeepHole2(IRModule module) {
        for (Function function : module.functions()) {
            if (function.isLibFunction()) continue;
            for (IList.INode<BasicBlock, Function> bbNode : function.getBbs()) {
                BasicBlock bb = bbNode.getValue();
                Instruction inst = bb.getLastInst();
                if (inst instanceof BrInst brInst
                        && brInst.getJudVal() != null
                        && !brInst.isJump()) {
                    BasicBlock trueBb = brInst.getTrueBlock();
                    BasicBlock falseBb = brInst.getFalseBlock();
                    if (trueBb == falseBb && trueBb != null) {
                        brInst.turnToJump(trueBb);
                    }
                }
            }
        }
    }

    private void PeepHole4(IRModule module) {
        for (Function function : module.functions()) {
            for (IList.INode<BasicBlock, Function> bbNode : function.getBbs()) {
                BasicBlock bb = bbNode.getValue();
                ArrayList<Instruction> memInsts = new ArrayList<>();
                for (IList.INode<Instruction, BasicBlock> instNode : bb.getInsts()) {
                    Instruction inst = instNode.getValue();
                    if (inst instanceof StoreInst || inst instanceof LoadInst) {
                        memInsts.add(inst);
                    }
                }

                for (int i = 0; i < memInsts.size() - 1; i++) {
                    Instruction inst = memInsts.get(i);
                    Instruction nxtInst = memInsts.get(i + 1);
                    if (inst instanceof StoreInst storeInst && nxtInst instanceof LoadInst loadInst
                            && storeInst.getPointer().equals(loadInst.getPointer())) {
                        Value value = storeInst.getValue();
                        loadInst.replaceUsedWith(value);
                        loadInst.removeSelf();
                    }
                }
            }
        }
    }


    @Override
    public String getName() {
        return "PeepHole";
    }
}
